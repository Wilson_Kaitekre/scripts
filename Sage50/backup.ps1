# Dernière modification: 2022-07-13
# Par: Wilson Laris Kaitekre (La fonction utilisée ici vient d'un personne autre que moi)
#           Source: https://techcommunity.microsoft.com/t5/itops-talk-blog/powershell-basics-how-to-delete-files-older-than-x-days/ba-p/1255317
# Desciption: Ce script permet de faire le backup des fichiers de compagnie Sage50 utilises par SquadFinance

$date = Get-Date -format "yyyyMMdd"
$DateStr = '{0:yyyyMMdd}' -f $Date


# Pour le dossier Dymo
$compress = @{
    Path = "C:\Dymo"
    CompressionLevel = "Fastest"
    DestinationPath = "C:\backup_temp\Dymo_$DateStr.Zip"
  }
  Compress-Archive @compress

# Pour le dossier cquintal
$compress = @{
    Path = "C:\Partages\Users\cquintal"
    CompressionLevel = "Fastest"
    DestinationPath = "C:\backup_temp\cquintal_$DateStr.Zip"
  }
  Compress-Archive @compress

# Pour le dossier CALCUL_BONI_CANADA_VIE
$compress = @{
    Path = "C:\Partages\CALCUL_BONI_CANADA_VIE"
    CompressionLevel = "Fastest"
    DestinationPath = "C:\backup_temp\CALCUL_BONI_CANADA_VIE_$DateStr.Zip"
  }
  Compress-Archive @compress

# Pour le dossier Sage50
$compress = @{
  Path = "C:\Partages\Sage50"
  CompressionLevel = "Fastest"
  DestinationPath = "C:\backup_temp\Sage50_$DateStr.Zip"
}
Compress-Archive @compress

# Pour le dossier Sage50-RFGM
$compress = @{
  Path = "C:\Partages\Sage50-RFGM"
  CompressionLevel = "Fastest"
  DestinationPath = "C:\backup_temp\Sage50-RFGM_$DateStr.Zip"
}
Compress-Archive @compress

# Ici nous allons maintenant copier dans le partage et vider le dossier backup_temp
mkdir \\div-ovh-mgmt.diversico.ca\Backup\$DateStr
move-item C:\backup_temp\* \\div-ovh-mgmt.diversico.ca\Backup\$DateStr
remove-item C:\backup_temp\*