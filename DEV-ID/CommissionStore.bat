:: Dernière modification: 2022-10-31
:: Par: Wilson Laris Kaitekre 
:: Desciption: Ce script permet de faire un upload du fichier de commissionStore vers le serveur

:: Vous devez seulement remplacer "Activitédétaillée.WDIV.2021310909nn57" avec les nouveau nom du fichier exemple "Activitédétaillée.WDIV.2022310909nn57".
:: Le nouveau fichier doit se retrouver à la même place que le fichier CommissionStore.bat
curl --fail --insecure --form "file=@.\Activitédétaillée.WDIV.2021310909nn57.csv" --user viriapi:l6TVoTzkL8th7ZXF3USu https://192.168.111.16:8001/import/upload