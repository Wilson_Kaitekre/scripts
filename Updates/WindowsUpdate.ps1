# Nous avons besoin de NuGet et PSWindowsUpdate module pour faire nos MAJ
# Check si NuGet est installé
if (!(Get-PackageProvider -Name NuGet -ErrorAction SilentlyContinue))
{
    Write-Host "NuGet is not installed, installing it now."
    Install-PackageProvider -Name NuGet -Force
}
else
{
    Write-Host "NuGet is already installed."
}

# Check si PSWindowsUpdate est installé
if (!(Get-Module -Name PSWindowsUpdate -ErrorAction SilentlyContinue))
{
    Write-Host "PSWindowsUpdate module is not installed, installing it now."
    Install-Module -Name PSWindowsUpdate -Force
}
else
{
    Write-Host "PSWindowsUpdate module is already installed."
}

# Disable le redemarrage automatique
$regKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update"
Set-ItemProperty -Path $regKey -Name "AUOptions" -Value "2"

# Install les updates
Get-WindowsUpdate -AcceptAll -Install -AutoReboot