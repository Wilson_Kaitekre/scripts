#Script permetant de faire l'iniialisation du dossier git sur les machines clientes
@echo off
if exist "c:\GitRepo" (
  rmdir /S /Q "c:\GitRepo"
)

git init --bare "c:\GitRepo"
cd "c:\GitRepo"
git clone https://Wilson_Kaitekre@bitbucket.org/Wilson_Kaitekre/scripts.git
attrib +h "c:\GitRepo"