# Chemin vers le fichier d'image que vous souhaitez définir comme fond d'écran
$imagePath = "C:\GitRepo\scripts\Display\MissionGCasa.png"

# Ajout du type pour l'API Windows
Add-Type @"
    using System;
    using System.Runtime.InteropServices;
    public class Wallpaper {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
    }
"@

# Définir les constantes pour SystemParametersInfo
$SPI_SETDESKWALLPAPER = 20
$SPIF_UPDATEINIFILE = 1
$SPIF_SENDWININICHANGE = 2

# Définir l'image comme fond d'écran
# WallpaperStyle à 10 pour l'étirer, 6 pour l'adapter, 2 pour la remplir, 0 pour centrer, 0 pour mosaïque
$RegistryKey = "HKCU:\Control Panel\Desktop"
Set-ItemProperty -Path $RegistryKey -Name WallpaperStyle -Value 6  # 6 pour adapter
Set-ItemProperty -Path $RegistryKey -Name TileWallpaper -Value 0

# Appliquer le fond d'écran
[Wallpaper]::SystemParametersInfo($SPI_SETDESKWALLPAPER, 0, $imagePath, $SPIF_UPDATEINIFILE -bor $SPIF_SENDWININICHANGE)