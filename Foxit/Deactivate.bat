:: Date de modification: 2022-02-23
:: Ce script permet de désactiver la license Foxit
:: Par: Wilson Laris Kaitekre

@echo off
echo Desactivation de Foxit...
echo VEUILLEZ REDEMARRER LA MACHINE APRES LA DESACTIVATION
cd C:\Program Files (x86)\Foxit Software\Foxit PDF Editor\
Activation.exe -cmdquietactive -deactivate