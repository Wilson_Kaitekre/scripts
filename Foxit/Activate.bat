:: Date de modification: 2022-05-26
:: Ce script permet d'activer la license Foxit
:: Par: Wilson Laris Kaitekre

:: Declaration des variables
set FoxitKey=%1

@echo off
echo Application de la licence Foxit...
echo VEUILLEZ REDEMARRER LA MACHINE APRES L'ACTIVATION
cd C:\Program Files (x86)\Foxit Software\Foxit PDF Editor\
Activation.exe -cmdquietactive "%FoxitKey%"