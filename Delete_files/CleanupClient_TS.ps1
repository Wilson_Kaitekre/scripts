# Dernière modification: 2022-03-09
# Par: Wilson Laris Kaitekre
# Desciption: Ce script permet d'enregister une tache planifiée pour faire un cleanup du desktop et download des utilisateurs
$action = New-ScheduledTaskAction -Execute "del_files.ps1"
$trigger = New-ScheduledTaskTrigger -Dayly -At "11:00 PM"
$principal = New-ScheduledTaskPrincipal -UserId 'diversico\support' -RunLevel Highest
$settings = New-ScheduledTaskSettingsSet
$task = New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger -Settings $settings

Register-ScheduledTask -TaskName "CLEAN_DOWNLOAD_DESKTOP" -InputObject $task
