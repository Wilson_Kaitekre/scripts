# Derni�re modification: 2022-05-27
# Par: Wilson Laris Kaitekre
# Desciption: Ce script permet de supprimer tous les fichiers du Downloads,Desktop et Corbeille
cd C:\Users\
$list_nom = dir -Name
foreach ($nom in $list_nom)
    {
        if($nom -notlike "Public")
        {
        Remove-item -path C:\Users\$nom\Desktop\* -Recurse -Force
        Remove-Item -path C:\Users\$nom\Downloads\* -Recurse -Force 
        Clear-RecycleBin -Force -Confirm
        }      
        
    }