# Dernière modification: 2022-05-27
# Par: Wilson Laris Kaitekre
# Desciption: Ce script permet de supprimer tous les fichiers du Downloads,Desktop, Documents et Corbeille

cd C:\Users\
$list_nom = dir -Name
foreach ($nom in $list_nom)
    {
        if($nom -notlike "Public")
        {
        Clear-RecycleBin -force
        Remove-item -path C:\Users\$nom\Desktop\* -Recurse -force
        Remove-Item -path C:\Users\$nom\Downloads\* -Recurse -Force 
        Remove-Item -path C:\Users\$nom\Documents\* -Recurse -Force 
        }      
        
    }
Copy-Item C:\Raccourci\* C:\Users\Public\Desktop\