Function RemMostApps {
        Write-Host "***Removing all apps and provisioned appx packages for this machine except Photos,Calculator...***"     
        Get-AppxPackage -AllUsers | where-object {$_.name -notlike "*Microsoft.Net.Native*" -and $_.name -notlike "*Calculator*" -and $_.name -notlike "*sticky*" -and $_.name -notlike "*Windows.Photos*" -and $_.name -notlike "*SoundRecorder*" -and $_.name -notlike "*MSPaint*"} | Remove-AppxPackage -erroraction silentlycontinue
        Get-AppxPackage -AllUsers | where-object {$_.name -notlike "*Microsoft.Net.Native*" -and $_.name -notlike "*Microsoft.VCLibs*" -and $_.name -notlike "*Calculator*" -and $_.name -notlike "*sticky*" -and $_.name -notlike "*Windows.Photos*" -and $_.name -notlike "*SoundRecorder*" -and $_.name -notlike "*MSPaint*"} | Remove-AppxPackage -erroraction silentlycontinue
        Get-AppxProvisionedPackage -online | where-object {$_.displayname -notlike "*Microsoft.Net.Native*" -and $_.displayname -notlike "*Microsoft.VCLibs*" -and $_.displayname -notlike "*Calculator*" -and $_.displayname -notlike "*sticky*" -and $_.displayname -notlike "*Windows.Photos*" -and $_.displayname -notlike "*SoundRecorder*" -and $_.displayname -notlike "*Netflix*" -and $_.displayname -notlike "*MSPaint*"} | Remove-AppxProvisionedPackage -online -erroraction silentlycontinue      
}   

RemMostApps